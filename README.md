# Make your Raspberry Pi file system read-only (Raspbian Buster)

This ansible playbook was inspired by a [post of *Andreas Schallwig*](https://medium.com/swlh/make-your-raspberry-pi-file-system-read-only-raspbian-buster-c558694de79) from 2020-09 at
[medium.com](https://medium.com) [1].

## Usage

1. Download `fs-read-only.yml` or clone this repository
1. Verify RPI is accessible
    ```sh
    ssh <hostname> 'sudo id'
    ```
1. Run playbook and apply changes to your RPI (via ssh...)
    ```sh
    ansible-playbook -i <hostname>, fs-read-only.yml
    ```

(Tested with `ansible 2.10.4`.)

## Known issues

### Failed to create temporary directory (/tmp)
There can be two reasons. 

1. First you have not cloned this repo and per default `ansible` tries to save its file to the users home directory. Because file-system at your RPi is read-only (after you have run this playbook before), you are not able to save any files there anymore. 
**Solution**: Remount your filesystem as writable
    ```sh
    ssh <hostname>
    sudo mount -o remount,rw / 
    # keep logged in and run ansible in a new session
    ```
1. By cloning this repo and change to the project directory, you are using `ansible.cfg`, which sets the `remote_dir` to `/tmp`. But this does not work currently, because the `tmpfs` for `/tmp` has wrong permissions.
**Solution**: Set correct permissions
    ```sh
    ssh <hostname> 'sudo chmod 1777 /tmp'
    ```

## Sources
[1] https://medium.com/swlh/make-your-raspberry-pi-file-system-read-only-raspbian-buster-c558694de79
